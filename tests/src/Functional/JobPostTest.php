<?php

namespace Drupal\Tests\contacts_jobs\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\user\Entity\User;

/**
 * Tests job posting features.
 *
 * @group contacts_jobs
 */
class JobPostTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'contacts_jobs',
  ];

  /**
   * Testing admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected User $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected function setUp() : void {
    parent::setUp();

    $this->adminUser = $this->createUser(['add job entities']);
    // This is cheating. Remove when we can.
    $this->adminUser->addRole('administrator');
    $this->adminUser->save();
  }

  /**
   * Test job post form access.
   */
  public function testViewDashboard() {
    // Check the site has installed successfully.
    $this->drupalGet('');
    $this->assertSession()->statusCodeEquals(200);

    // Check that prior to logging in, we can't access the job post form.
    $this->drupalGet('job/post');
    $this->assertSession()->statusCodeEquals(403);

    $this->drupalLogin($this->adminUser);

    // Check that the job post form has the expected content.
    $this->drupalGet('job/post');
    $session = $this->assertSession();

    $session->statusCodeEquals(200);
  }

}
