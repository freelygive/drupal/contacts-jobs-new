<?php

namespace Drupal\Tests\contacts_jobs\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the new entity API for the test field type.
 *
 * @group field
 */
class JobWithdrawnTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'system',
    'user',
    'views',
    'contacts_jobs',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['contacts_jobs']);
    $this->installEntitySchema('view');
    $this->installEntitySchema('contacts_job');
    $this->installSchema('system', 'sequences');
  }

  /**
   * Tests using entity fields of the test field type.
   */
  public function testTestItem() {
    $time = \Drupal::time()->getRequestTime();
    $time_last_week = $time - 604800;
    $time_in_a_week = $time + 604800;
    $values = [
      'type' => 'contacts_job',
    ];

    /** @var \Drupal\contacts_jobs\Entity\JobInterface $job */
    $job = \Drupal::entityTypeManager()->getStorage('contacts_job')->create($values);
    $job->setClosingTime($time_in_a_week);
    $job->save();

    $this->assertNull($job->getWithdrawnTime());
    $this->assertFalse($job->isWithdrawn());
    $this->assertFalse($job->isPublished());

    // Time might have changed since start of test.
    $withdrawn_time = time();
    $job->setWithdrawn(TRUE);
    $job->save();

    $this->assertNull($job->getWithdrawnTime());
    $this->assertTrue($job->isWithdrawn());
    $this->assertFalse($job->isPublished());

    $job->setPublishStartTime($time_last_week);
    $job->save();

    $this->assertEquals($withdrawn_time, $job->getWithdrawnTime());
    $this->assertTrue($job->isWithdrawn());
    $this->assertFalse($job->isPublished());

    $job->setWithdrawn(FALSE);
    $job->save();

    $this->assertNull($job->getWithdrawnTime());
    $this->assertFalse($job->isWithdrawn());
    $this->assertTrue($job->isPublished());
  }

}
