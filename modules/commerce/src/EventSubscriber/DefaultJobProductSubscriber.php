<?php

namespace Drupal\contacts_jobs_commerce\EventSubscriber;

use Drupal\contacts_jobs_commerce\Event\JobProductVariationEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * The default job product subscriber.
 */
class DefaultJobProductSubscriber implements EventSubscriberInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs event subscriber.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Get the job posting, if there is only a single product.
   *
   * @param \Drupal\contacts_jobs_commerce\Event\JobProductVariationEvent $event
   *   The job product variation event.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getOnlyJob(JobProductVariationEvent $event) {
    $storage = $this->entityTypeManager->getStorage('commerce_product');
    $ids = $storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('type', 'contacts_job_posting')
      ->range(0, 2)
      ->execute();

    if (count($ids) === 1) {
      $event->allowPostTypes($ids);
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      // Get the default job, running last.
      JobProductVariationEvent::NAME => ['getOnlyJob', 999],
    ];
  }

}
