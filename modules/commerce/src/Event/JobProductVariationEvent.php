<?php

namespace Drupal\contacts_jobs_commerce\Event;

use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\Component\EventDispatcher\Event;
use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;

/**
 * Event class for determining the product variation to use for the posting.
 */
class JobProductVariationEvent extends Event {

  /**
   * The event name.
   */
  const NAME = 'contacts_jobs_commerce_get_product_variation';

  /**
   * Indicates that the specified variation suffix is preferred.
   */
  const VARIATION_PREFERRED = 'preferred';

  /**
   * Indicates the specified variation suffix is required.
   */
  const VARIATION_REQUIRED = 'required';

  /**
   * The workflow used.
   *
   * @var string
   */
  protected string $workflow;

  /**
   * The job being posted.
   *
   * @var \Drupal\contacts_jobs\Entity\JobInterface
   */
  protected JobInterface $job;

  /**
   * Products that are allowed.
   *
   * @var int[]
   */
  protected array $allowedProductIds = [];

  /**
   * Products that are dis-allowed.
   *
   * @var int[]
   */
  protected array $disallowedProductIds = [];

  /**
   * The variation SKU suffix.
   *
   * @var string
   */
  protected string $variationSuffix;

  /**
   * The strength of the variation suffix requirement.
   *
   * @var string
   */
  protected string $variationStrength = self::VARIATION_PREFERRED;

  /**
   * The product entity storage.
   *
   * @var \Drupal\Core\Entity\ContentEntityStorageInterface
   */
  protected ContentEntityStorageInterface $storage;

  /**
   * The currency code we are interested in.
   *
   * @var string|null
   */
  protected string|null $currencyCode = NULL;

  /**
   * Construct the event.
   *
   * @param \Drupal\contacts_jobs\Entity\JobInterface $job
   *   The job being posted.
   * @param string $workflow
   *   The workflow we are using.
   * @param \Drupal\Core\Entity\ContentEntityStorageInterface $storage
   *   The product entity storage handler.
   */
  public function __construct(JobInterface $job, string $workflow, ContentEntityStorageInterface $storage) {
    $this->job = $job;
    $this->workflow = $workflow;
    $this->variationSuffix = "-{$workflow}";
    $this->storage = $storage;
  }

  /**
   * Get the required currency code, if any.
   *
   * @return string|null
   *   The currency code.
   */
  public function getCurrencyCode(): ?string {
    return $this->currencyCode;
  }

  /**
   * Set the currency code.
   *
   * @param string|null $currency_code
   *   The currency code.
   *
   * @return $this
   */
  public function setCurrencyCode(null|string $currency_code): static {
    $this->currencyCode = $currency_code;

    return $this;
  }

  /**
   * Get the workflow.
   *
   * @return string
   *   The workflow.
   */
  public function getWorkflow(): string {
    return $this->workflow;
  }

  /**
   * Get the job being posted.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The job.
   */
  public function getJob(): JobInterface {
    return $this->job;
  }

  /**
   * Get the variation suffix.
   *
   * @return string
   *   The variation suffix.
   */
  public function getVariationSuffix(): string {
    return $this->variationSuffix;
  }

  /**
   * Set the variation suffix.
   *
   * @param string|null $variationSuffix
   *   The variation suffix or NULL for none.
   *
   * @return $this
   */
  public function setVariationSuffix(?string $variationSuffix): JobProductVariationEvent {
    $this->variationSuffix = $variationSuffix;
    return $this;
  }

  /**
   * Get the strength of the variation suffix.
   *
   * @return string
   *   One of the self::VARIATION_* constants.
   */
  public function getVariationStrength(): string {
    return $this->variationStrength;
  }

  /**
   * Set the strength of the variation suffix.
   *
   * @param string $variationStrength
   *   One of the self::VARIATION_* constants.
   *
   * @return $this
   */
  public function setVariationStrength(string $variationStrength): JobProductVariationEvent {
    $this->variationStrength = $variationStrength;
    return $this;
  }

  /**
   * Allowed post types.
   *
   * @param int[] $product_ids
   *   The product IDs to allow.
   * @param bool $override_disallowed
   *   Whether to override a previous dis-allowance. Defaults to FALSE.
   *
   * @return $this
   */
  public function allowPostTypes(array $product_ids, bool $override_disallowed = FALSE): JobProductVariationEvent {
    // Clear from dis-allowed if overriding.
    if ($override_disallowed) {
      $this->disallowedProductIds = array_diff($this->disallowedProductIds, $product_ids);
    }
    // Otherwise, remove disallowed items.
    else {
      $product_ids = array_diff($product_ids, $this->disallowedProductIds);
    }

    if (!empty($product_ids)) {
      // Remove anything that would cause a duplicate.
      $product_ids = array_diff($product_ids, $this->allowedProductIds);

      // Add in the remaining product IDs.
      if (!empty($product_ids)) {
        $this->allowedProductIds = array_merge($this->allowedProductIds, $product_ids);
      }
    }

    return $this;
  }

  /**
   * Dis-allow post types.
   *
   * @param int[] $product_ids
   *   The product ID to dis-allow.
   *
   * @return $this
   */
  public function disallowPostTypes(array $product_ids): JobProductVariationEvent {
    // Remove anything that would cause a duplicate.
    $product_ids = array_diff($product_ids, $this->disallowedProductIds);

    if (!empty($product_ids)) {
      // Add in the remaining product IDs.
      $this->disallowedProductIds = array_merge($this->disallowedProductIds, $product_ids);

      // Clean up the allowed list.
      $this->allowedProductIds = array_diff($this->allowedProductIds, $this->disallowedProductIds);
    }

    return $this;
  }

  /**
   * Get the product variation to use.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface|null
   *   The variation, or NULL if there is nothing suitable.
   */
  public function getProductVariation(): ?ProductVariationInterface {
    $variations = $this->getProductVariations(['contacts_job_posting']);
    return reset($variations) ?: NULL;
  }

  /**
   * Get the available product variations.
   *
   * @param array $product_types
   *   Filter the returned variations to specific product types.
   *
   * @return \Drupal\commerce_product\Entity\ProductVariationInterface[]
   *   The product variations, keyed by product ID.
   */
  public function getProductVariations(array $product_types = []): array {
    /** @var \Drupal\commerce_product\Entity\ProductInterface[] $products */
    $products = $this->storage->loadMultiple($this->allowedProductIds);
    $variations = [];

    // Loop over the allowed products.
    foreach ($products as $product_id => $product) {
      // Get all the variations for this product and filter them. We will also
      // explicitly track a matching suffix.
      $variation_with_suffix = NULL;

      // We use an anonymous function as we need to use $variation_with_suffix,
      // which can't be done via a method as the callback. However, we do want
      // to use a callback so extending classes can override the filter.
      $product_variations = array_filter($product->getVariations(), function (ProductVariationInterface $variation) use (&$variation_with_suffix, $product_types) {
        return $this->filterVariation($variation, $variation_with_suffix, $product_types);
      });

      // If we have a suffix match, use it.
      if ($variation_with_suffix) {
        $variations[$product_id] = $variation_with_suffix;
      }
      // Otherwise, if we have any matches and are only preferring the suffix,
      // we use the first of our matches.
      elseif ($this->variationStrength === self::VARIATION_PREFERRED && $product_variations) {
        $variations[$product_id] = reset($product_variations);
      }
    }

    return $variations;
  }

  /**
   * Check whether a variation is suitable.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   The variation we are checking.
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface|null $variation_with_suffix
   *   Variable by reference to store the first variation that matches the
   *   specified suffix.
   * @param array $product_types
   *   Filter the returned variations to specific product types.
   *
   * @return bool
   *   Whether the variation should be included in the possible matches.
   */
  protected function filterVariation(ProductVariationInterface $variation, ?ProductVariationInterface &$variation_with_suffix, array $product_types = []): bool {
    // Only include published products.
    if (!$variation->isPublished() || (!empty($product_types) && !in_array($variation->getProduct()->bundle(), $product_types))) {
      return FALSE;
    }

    // Filter out incorrect currency codes.
    if ($this->currencyCode) {
      $price = $variation->getPrice();
      if ($price && $price->getCurrencyCode() !== $this->currencyCode) {
        return FALSE;
      }
    }

    // Ensure we have access.
    if (!$variation->access('view')) {
      return FALSE;
    }

    // If we have an exact match on suffix, track it.
    if ($this->variationSuffix && str_ends_with($variation->getSku(), $this->variationSuffix)) {
      $variation_with_suffix = $variation;
    }

    return TRUE;
  }

}
