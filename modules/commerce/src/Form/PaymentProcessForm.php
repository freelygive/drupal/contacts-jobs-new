<?php

namespace Drupal\contacts_jobs_commerce\Form;

use Drupal\commerce\InlineFormManager;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentGatewayInterface;
use Drupal\commerce_payment\Exception\DeclineException;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\ManualPaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OnsitePaymentGatewayInterface;
use Drupal\Component\Datetime\Time;
use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\contacts_jobs\Form\JobProgressTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Job payment forms.
 *
 * @ingroup contacts_jobs
 */
class PaymentProcessForm extends FormBase {

  use JobProgressTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected InlineFormManager $inlineFormManager;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface|null
   */
  protected ?OrderInterface $order = NULL;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected Time $time;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->entityTypeManager = $container->get('entity_type.manager');
    $form->logger = $container->get('logger.channel.commerce_payment');
    $form->inlineFormManager = $container->get('plugin.manager.commerce_inline_form');
    $form->setMessenger($container->get('messenger'));
    $form->time = $container->get('datetime.time');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_jobs_commerce_payment_process';
  }

  /**
   * {@inheritdoc}
   */
  protected function getActiveStep(): string {
    return 'pay';
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\contacts_jobs\Entity\JobInterface|null $contacts_job
   *   The job we are paying form.
   * @param \Drupal\Core\Routing\RouteMatchInterface|null $route_match
   *   The current route match.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *   The form structure.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?JobInterface $contacts_job = NULL, ?RouteMatchInterface $route_match = NULL) {
    $form['#id'] = 'job-payment-form';

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Continue'),
    ];

    if (!$contacts_job) {
      throw new \InvalidArgumentException('Job is required for the payment form.');
    }
    $this->job = $contacts_job;

    $this->getWorkflowFromRoute($route_match);
    $this->buildProgress($form);

    $this->getOrderForJob($contacts_job);

    // The payment gateway is currently always required to be set.
    if ($this->order->get('payment_gateway')->isEmpty()) {
      return $this->redirectError($this->t('No payment gateway selected.'));
    }

    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $this->order->payment_gateway->entity;
    $payment_gateway_plugin = $payment_gateway->getPlugin();

    $payment = $this->createPayment($payment_gateway);

    if ($payment_gateway_plugin instanceof OnsitePaymentGatewayInterface) {
      try {
        $payment->payment_method = $this->order->payment_method->entity;
        $payment_gateway_plugin->createPayment($payment);

        if ($payment->isCompleted()) {
          $this->job->setPublishStartTime($this->time->getRequestTime())->save();
          $this->messenger()->addStatus($this->t('Thank you for your payment. Your job has been published.'));
        }
        else {
          $this->messenger()->addStatus($this->t('Thank you for your payment. Your job will be published when your payment is confirmed.'));
        }

        return $this->redirectComplete();
      }
      catch (DeclineException $e) {
        return $this->redirectError();
      }
      catch (PaymentGatewayException $e) {
        $this->logger->error($e->getMessage());
        return $this->redirectError();
      }
    }
    elseif ($payment_gateway_plugin instanceof OffsitePaymentGatewayInterface) {
      $inline_form = $this->inlineFormManager->createInstance('payment_gateway_form', [
        'operation' => 'offsite-payment',
        'catch_build_exceptions' => FALSE,
      ], $payment);

      $form['offsite_payment'] = [
        '#parents' => array_merge($form['#parents'] ?? [], ['offsite_payment']),
        '#inline_form' => $inline_form,
        '#return_url' => $this->buildReturnUrl()->toString(),
        '#cancel_url' => $this->buildCancelUrl()->toString(),
        '#capture' => TRUE,
      ];
      try {
        $form['offsite_payment'] = $inline_form->buildInlineForm($form['offsite_payment'], $form_state);
      }
      catch (PaymentGatewayException $e) {
        $this->logger->error($e->getMessage());
        return $this->redirectError();
      }

      return $form;
    }
    elseif ($payment_gateway_plugin instanceof ManualPaymentGatewayInterface) {
      try {
        $payment_gateway_plugin->createPayment($payment);
        $this->messenger()->addStatus($this->t('Thank you for your payment. Your job will be published when your payment is confirmed.'));
        return $this->redirectComplete();
      }
      catch (PaymentGatewayException $e) {
        $this->logger->error($e->getMessage());
        return $this->redirectError();
      }
    }
    else {
      return $this->redirectError();
    }
  }

  /**
   * Builds the URL to the "return" page.
   *
   * @return \Drupal\Core\Url
   *   The "return" page URL.
   */
  protected function buildReturnUrl(): Url {
    return Url::fromRoute('contacts_jobs_commerce.payment.return', [
      'commerce_order' => $this->order->id(),
      'workflow' => $this->workflow,
    ], ['absolute' => TRUE]);
  }

  /**
   * Builds the URL to the "cancel" page.
   *
   * @return \Drupal\Core\Url
   *   The "cancel" page URL.
   */
  protected function buildCancelUrl(): Url {
    return Url::fromRoute('contacts_jobs_commerce.payment.cancel', [
      'commerce_order' => $this->order->id(),
      'workflow' => $this->workflow,
    ], ['absolute' => TRUE]);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {}

  /**
   * Get the order for the job.
   *
   * @param \Drupal\contacts_jobs\Entity\JobInterface $job
   *   The job.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getOrderForJob(JobInterface $job): void {
    if ($this->order) {
      return;
    }

    $storage = $this->entityTypeManager
      ->getStorage('commerce_order');
    $orders = $storage
      ->loadByProperties([
        'type' => 'contacts_job',
        'contacts_job' => $job->id(),
        'state' => 'draft',
      ]);
    /** @var \Drupal\commerce_order\Entity\OrderInterface|false $this->order */
    $this->order = reset($orders);
    if (!$this->order) {
      throw new \InvalidArgumentException('Unable to find the order for this job.');
    }
  }

  /**
   * Redirect back a step on error.
   *
   * @param mixed $message
   *   The error message, or FALSE for none. If NULL, a generic message will be
   *   used.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirection response.
   */
  protected function redirectError(mixed $message = NULL) {
    if (!isset($message)) {
      $message = $this->t('We encountered an unexpected error processing your payment. Please try again later.');
    }
    if ($message) {
      $this->messenger()->addError($message);
    }

    $params = [
      'contacts_job' => $this->job->id(),
    ];

    if ($this->workflow) {
      $params['workflow'] = $this->workflow;
    }

    return $this->redirect('contacts_jobs_commerce.contacts_job.payment', $params);
  }

  /**
   * Redirect to the job on completion.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   The redirection response.
   */
  protected function redirectComplete() {
    // Mark the order as placed.
    $this->order->getState()->applyTransitionById('place');
    $this->order->save();

    return $this->redirect('entity.contacts_job.canonical', [
      'contacts_job' => $this->job->id(),
    ]);
  }

  /**
   * Create the payment to be processed.
   *
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway
   *   The payment gateway in use.
   *
   * @return \Drupal\commerce_payment\Entity\PaymentInterface
   *   The newly created payment.
   */
  protected function createPayment(PaymentGatewayInterface $payment_gateway) {
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $payment_storage->create([
      'state' => 'new',
      'amount' => $this->order->getBalance(),
      'payment_gateway' => $payment_gateway->id(),
      'order_id' => $this->order->id(),
    ]);
    return $payment;
  }

}
