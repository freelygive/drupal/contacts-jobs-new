<?php

namespace Drupal\contacts_jobs_commerce\Form;

use CommerceGuys\Intl\Formatter\CurrencyFormatterInterface;
use Drupal\commerce\InlineFormManager;
use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\PaymentOption;
use Drupal\commerce_payment\PaymentOptionsBuilderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\SupportsStoredPaymentMethodsInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\NestedArray;
use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\contacts_jobs\Form\JobProgressTrait;
use Drupal\contacts_jobs_commerce\Event\JobProductVariationEvent;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Form controller for Job payment forms.
 *
 * @ingroup contacts_jobs
 */
class PaymentDetailsForm extends FormBase {

  use JobProgressTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The currency formatter.
   *
   * @var \CommerceGuys\Intl\Formatter\CurrencyFormatterInterface
   */
  protected CurrencyFormatterInterface $currencyFormatter;

  /**
   * The payment options builder.
   *
   * @var \Drupal\commerce_payment\PaymentOptionsBuilderInterface
   */
  protected PaymentOptionsBuilderInterface $paymentOptionsBuilder;

  /**
   * The inline form manager.
   *
   * @var \Drupal\commerce\InlineFormManager
   */
  protected InlineFormManager $inlineFormManager;

  /**
   * The event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The job board commerce config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The order.
   *
   * @var \Drupal\commerce_order\Entity\OrderInterface|bool
   */
  protected OrderInterface|bool $order = FALSE;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $form = parent::create($container);
    $form->entityTypeManager = $container->get('entity_type.manager');
    $form->currencyFormatter = $container->get('commerce_price.currency_formatter');
    $form->paymentOptionsBuilder = $container->get('commerce_payment.options_builder');
    $form->inlineFormManager = $container->get('plugin.manager.commerce_inline_form');
    $form->eventDispatcher = $container->get('event_dispatcher');
    $form->config = $container->get('config.factory')->get('contacts_jobs_commerce.settings');
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_jobs_commerce_payment';
  }

  /**
   * {@inheritdoc}
   */
  protected function getActiveStep(): string {
    return 'pay';
  }

  /**
   * Helper to get the related order.
   *
   * @return \Drupal\commerce_order\Entity\OrderInterface
   *   Returns the related order.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function getOrder(): OrderInterface {
    if (!$this->order) {
      $this->getOrderForJob($this->job);
    }
    return $this->order;
  }

  /**
   * Form constructor.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   * @param \Drupal\contacts_jobs\Entity\JobInterface|null $contacts_job
   *   The job we are paying form.
   * @param \Drupal\Core\Routing\RouteMatchInterface|null $route_match
   *   The current route match.
   *
   * @return array
   *   The form structure.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\TypedData\Exception\MissingDataException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function buildForm(array $form, FormStateInterface $form_state, ?JobInterface $contacts_job = NULL, ?RouteMatchInterface $route_match = NULL): array {
    $form['#id'] = 'job-payment-form';
    $form['#parents'] = (isset($form['#parents'])) ? $form['#parents'] : [];

    if (!$contacts_job) {
      throw new \InvalidArgumentException('Job is required for the payment form.');
    }
    $this->job = $contacts_job;

    $this->getWorkflowFromRoute($route_match);
    $this->buildProgress($form);

    $this->getOrderForJob($contacts_job);

    // The only reason we cant get an order is if the no product can be found.
    if (!$this->order) {
      $this->logger('contacts_jobs_commerce')
        ->error('Unable to determine product for %job [@id].', [
          '%job' => $contacts_job->label(),
          '@id' => $contacts_job->id(),
        ]);

      $this->messenger()->addError($this->t('Sorry, something unexpected went wrong. Please try again later.'));
      return [];
    }

    $price = $this->order->getTotalPrice();
    $items = $this->order->get('order_items')->referencedEntities();
    $item = $items[0];
    $form['order_description'] = [
      '#type' => 'item',
      '#title' => $this->t('You are buying'),
      '#markup' => new FormattableMarkup('@label - @price', [
        '@label' => $item->label(),
        '@price' => $this->currencyFormatter->format($price->getNumber(), $price->getCurrencyCode()),
      ]),
    ];

    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    // Load the payment gateways. This fires an event for filtering the
    // available gateways, and then evaluates conditions on all remaining ones.
    $payment_gateways = $payment_gateway_storage->loadMultipleForOrder($this->order);
    // Can't proceed without any payment gateways.
    if (empty($payment_gateways)) {
      $this->messenger()->addError($this->t('There are no payment gateways available for this order. Please try again later.'));
      return $form;
    }

    // Core bug #1988968 doesn't allow the payment method add form JS to depend
    // on an external library, so the libraries need to be preloaded here.
    foreach ($payment_gateways as $payment_gateway) {
      foreach ($payment_gateway->getPlugin()->getLibraries() as $library) {
        $form['#attached']['library'][] = $library;
      }
    }

    $option_data = $this->getPaymentOptionAndLabels($form_state, $payment_gateways);
    $options = $option_data['options'];
    $option_labels = $option_data['option_labels'];
    $default_option = $option_data['default_option'];

    $form['payment_method'] = [
      '#type' => 'radios',
      '#title' => $this->t('Payment method'),
      '#options' => $option_labels,
      '#default_value' => $default_option->getId(),
      '#ajax' => [
        'callback' => [get_class($this), 'ajaxRefresh'],
        'wrapper' => $form['#id'],
      ],
      '#access' => count($options) > 1,
    ];
    // Add a class to each individual radio, to help themers.
    foreach ($options as $option) {
      $class_name = $option->getPaymentMethodId() ? 'stored' : 'new';
      $form['payment_method'][$option->getId()]['#attributes']['class'][] = "payment-method--$class_name";
    }
    // Store the options for submitPaneForm().
    $form['#payment_options'] = $options;

    $default_payment_gateway_id = $default_option->getPaymentGatewayId();
    $payment_gateway = $payment_gateways[$default_payment_gateway_id];
    if ($payment_gateway->getPlugin() instanceof SupportsStoredPaymentMethodsInterface) {
      $form = $this->buildPaymentMethodForm($form, $form_state, $default_option);
    }
    else {
      $form = $this->buildBillingProfileForm($form, $form_state);
    }

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Make payment'),
    ];

    return $form;
  }

  /**
   * Builds the payment method form for the selected payment option.
   *
   * @param array $form
   *   The pane form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   * @param \Drupal\commerce_payment\PaymentOption $payment_option
   *   The payment option.
   *
   * @return array
   *   The modified pane form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildPaymentMethodForm(array $form, FormStateInterface $form_state, PaymentOption $payment_option): array {
    if ($payment_option->getPaymentMethodId() && !$payment_option->getPaymentMethodTypeId()) {
      // Editing payment methods at checkout is not supported.
      return $form;
    }

    /** @var \Drupal\commerce_payment\PaymentMethodStorageInterface $payment_method_storage */
    $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
    $payment_method = $payment_method_storage->create([
      'type' => $payment_option->getPaymentMethodTypeId(),
      'payment_gateway' => $payment_option->getPaymentGatewayId(),
      'uid' => $this->order->getCustomerId(),
      'billing_profile' => $this->order->getBillingProfile(),
      'reusable' => TRUE,
    ]);
    $inline_form = $this->inlineFormManager->createInstance('payment_gateway_form', [
      'operation' => 'add-payment-method',
    ], $payment_method);

    $form['add_payment_method'] = [
      '#parents' => array_merge($form['#parents'], ['add_payment_method']),
      '#inline_form' => $inline_form,
      '#allow_reusable' => TRUE,
      '#always_save' => TRUE,
      '#free_orders' => $this->config->get('payment_methods_free_orders'),
    ];
    $form['add_payment_method'] = $inline_form->buildInlineForm($form['add_payment_method'], $form_state);

    return $form;
  }

  /**
   * Builds the billing profile form.
   *
   * @param array $form
   *   The pane form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state of the parent form.
   *
   * @return array
   *   The modified pane form.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function buildBillingProfileForm(array $form, FormStateInterface $form_state) {
    $billing_profile = $this->order->getBillingProfile();
    if (!$billing_profile) {
      $billing_profile = $this->entityTypeManager->getStorage('profile')->create([
        'uid' => $this->order->getCustomerId(),
        'type' => 'customer',
      ]);
    }
    $inline_form = $this->inlineFormManager->createInstance('customer_profile', [
      'profile_scope' => 'billing',
      'available_countries' => $this->order->getStore()->getBillingCountries(),
    ], $billing_profile);

    $form['billing_information'] = [
      '#parents' => array_merge($form['#parents'] ?? [], ['billing_information']),
      '#inline_form' => $inline_form,
    ];
    $form['billing_information'] = $inline_form->buildInlineForm($form['billing_information'], $form_state);

    return $form;
  }

  /**
   * Ajax callback.
   */
  public static function ajaxRefresh(array $form, FormStateInterface $form_state) {
    $parents = $form_state->getTriggeringElement()['#parents'];
    array_pop($parents);
    return NestedArray::getValue($form, $parents);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if ($this->order->isPaid() || $this->order->getTotalPrice()->isZero()) {
      return;
    }

    $values = $form_state->getValue($form['#parents']);
    if (!isset($values['payment_method'])) {
      $form_state->setError($form, $this->t('There are no payment gateways available for this order. Please try again later.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $billing_profile = NULL;
    if (isset($form['billing_information'])) {
      /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
      $inline_form = $form['billing_information']['#inline_form'];
      /** @var \Drupal\profile\Entity\ProfileInterface $billing_profile */
      $billing_profile = $inline_form->getEntity();
      if ($this->order->isPaid() || $this->order->getTotalPrice()->isZero()) {
        $this->order->setBillingProfile($billing_profile);
        $this->order->save();
        return;
      }
    }

    $values = $form_state->getValues();
    /** @var \Drupal\commerce_payment\PaymentOption $selected_option */
    $selected_option = $form['#payment_options'][$values['payment_method']];
    /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
    $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
    /** @var \Drupal\commerce_payment\Entity\PaymentGatewayInterface $payment_gateway */
    $payment_gateway = $payment_gateway_storage->load($selected_option->getPaymentGatewayId());
    if (!$payment_gateway) {
      return;
    }

    if ($payment_gateway->getPlugin() instanceof SupportsStoredPaymentMethodsInterface) {
      if (!empty($selected_option->getPaymentMethodTypeId())) {
        /** @var \Drupal\commerce\Plugin\Commerce\InlineForm\EntityInlineFormInterface $inline_form */
        $inline_form = $form['add_payment_method']['#inline_form'];
        // The payment method was just created.
        $payment_method = $inline_form->getEntity();
      }
      else {
        /** @var \Drupal\commerce_payment\PaymentMethodStorageInterface $payment_method_storage */
        $payment_method_storage = $this->entityTypeManager->getStorage('commerce_payment_method');
        $payment_method = $payment_method_storage->load($selected_option->getPaymentMethodId());
      }

      /** @var \Drupal\commerce_payment\Entity\PaymentMethodInterface $payment_method */
      $this->order->set('payment_gateway', $payment_method->getPaymentGateway());
      $this->order->set('payment_method', $payment_method);
      $this->order->setBillingProfile($payment_method->getBillingProfile());
    }
    else {
      $this->order->set('payment_gateway', $payment_gateway);
      $this->order->set('payment_method', NULL);
      $this->order->setBillingProfile($billing_profile);
    }

    $this->order->save();
    $form_state->setRedirect('contacts_jobs_commerce.contacts_job.payment_process', [
      'contacts_job' => $this->job->id(),
      'workflow' => $this->workflow,
    ]);
  }

  /**
   * Get the order for the job.
   *
   * @param \Drupal\contacts_jobs\Entity\JobInterface $job
   *   The job.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function getOrderForJob(JobInterface $job): void {
    if ($this->order) {
      return;
    }

    $storage = $this->entityTypeManager
      ->getStorage('commerce_order');
    $orders = $storage
      ->loadByProperties([
        'type' => 'contacts_job',
        'contacts_job' => $job->id(),
        'state' => 'draft',
      ]);
    /** @var \Drupal\commerce_order\Entity\OrderInterface|false $this->order */
    $this->order = reset($orders);

    if (!$this->order) {
      // Trigger the product variation event to determine what to use.
      $event = new JobProductVariationEvent($job, $this->workflow, $this->entityTypeManager->getStorage('commerce_product'));
      $this->eventDispatcher->dispatch($event, $event::NAME);
      $variation = $event->getProductVariation();
      // Don't create the order if there isn't a variation.
      if (!$variation) {
        return;
      }

      /** @var \Drupal\commerce_order\Entity\OrderInterface $this->order */
      $this->order = $storage->create([
        'type' => 'contacts_job',
        'store_id' => $this->getStoreId(),
        'contacts_job' => $job->id(),
        'state' => 'draft',
        'uid' => $this->currentUser()->id(),
      ]);
      $this->order->save();

      /** @var \Drupal\commerce_order\Entity\OrderItemInterface $order_item */
      $order_item = $this->entityTypeManager
        ->getStorage('commerce_order_item')
        ->create([
          'type' => $variation->getOrderItemTypeId(),
          'purchased_entity' => $variation->id(),
          'quantity' => 1,
          'order_id' => $this->order->id(),
        ]);
      $order_item->setTitle($variation->getOrderItemTitle());
      if ($price = $variation->getPrice()) {
        $order_item->setUnitPrice($price);
      }
      $order_item->save();
      $this->order->addItem($order_item);
      $this->order->save();
    }
  }

  /**
   * Helper to check if the order balance is positive.
   *
   * @return bool
   *   Whether the order balance is positive.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function isBalancePositive(): bool {
    if (!$this->order) {
      $this->getOrderForJob($this->job);
    }

    return $this->order->getBalance()->isPositive();
  }

  /**
   * Get the store ID for the job order.
   *
   * @return int|null
   *   The store ID, if there is one.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getStoreId(): ?int {
    // Check our config.
    if ($store_id = $this->config->get('store_id')) {
      return $store_id;
    }

    // Get the default.
    /** @var \Drupal\commerce_store\StoreStorageInterface $store_storage */
    $store_storage = $this->entityTypeManager->getStorage('commerce_store');
    if ($store = $store_storage->loadDefault()) {
      return $store->id();
    }

    return NULL;
  }

  /**
   * Helper to get the default payment option and labels.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The Form State.
   * @param \Drupal\commerce_payment\Entity\PaymentGatewayInterface[] $payment_gateways
   *   The valid payment gateways. Loaded from order if empty.
   *
   * @return array
   *   An array of 'options' => [], 'default_option' => , 'option_labels' => []
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getPaymentOptionAndLabels(FormStateInterface $form_state, array $payment_gateways = []): array {
    if (empty($payment_gateways)) {
      /** @var \Drupal\commerce_payment\PaymentGatewayStorageInterface $payment_gateway_storage */
      $payment_gateway_storage = $this->entityTypeManager->getStorage('commerce_payment_gateway');
      // Load the payment gateways. This fires an event for filtering the
      // available gateways, and then evaluates conditions on all remaining
      // ones.
      $payment_gateways = $payment_gateway_storage->loadMultipleForOrder($this->order);
    }

    $options = $this->paymentOptionsBuilder->buildOptions($this->order, $payment_gateways);
    $option_labels = array_map(function (PaymentOption $option) {
      return $option->getLabel();
    }, $options);
    $default_option_id = NestedArray::getValue($form_state->getUserInput(), ['payment_method']);
    if ($default_option_id && isset($options[$default_option_id])) {
      $default_option = $options[$default_option_id];
    }
    else {
      $default_option = $this->paymentOptionsBuilder->selectDefaultOption($this->order, $options);
    }

    return [
      'options' => $options,
      'default_option' => $default_option,
      'option_labels' => $option_labels,
    ];
  }

}
