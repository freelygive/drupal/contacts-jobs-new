<?php

namespace Drupal\contacts_jobs_commerce\Form;

use Drupal\commerce_store\StoreStorageInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure contacts_jobs_commerce settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The store storage.
   *
   * @var \Drupal\commerce_store\StoreStorageInterface
   */
  protected $storeStorage;

  /**
   * Constructs a new BookingSettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\commerce_store\StoreStorageInterface $store_storage
   *   The commerce store entity storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StoreStorageInterface $store_storage) {
    parent::__construct($config_factory);
    $this->storeStorage = $store_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')->getStorage('commerce_store')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'contacts_jobs_commerce_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['contacts_jobs_commerce.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('contacts_jobs_commerce.settings');

    $form['store_id'] = [
      '#type' => 'commerce_entity_select',
      '#title' => $this->t('Job board store'),
      '#description' => $this->t('Which store to associate with job postings.'),
      '#target_type' => 'commerce_store',
      '#default_value' => $config->get('store_id'),
      '#required' => TRUE,
      '#hide_single_entity' => FALSE,
    ];

    $form['payment_methods_free_orders'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Free products available to purchase'),
      '#description' => $this->t('Check this box iof your store includes products with no charge'),
      '#default_value' => $config->get('payment_methods_free_orders'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('contacts_jobs_commerce.settings')
      ->set('store_id', $form_state->getValue('store_id'))
      ->set('payment_methods_free_orders', $form_state->getValue('payment_methods_free_orders'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
