<?php

namespace Drupal\contacts_jobs;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Access controller for the Job entity.
 *
 * @see \Drupal\contacts_jobs\Entity\Job.
 */
class JobAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\contacts_jobs\Entity\JobInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished job entities')
            ->orIf(
              AccessResult::allowedIfHasPermission($account, 'edit own job entities')
                ->andIf(
                  AccessResult::allowedIf($entity->getOwnerId() == $account->id())
                    ->addCacheContexts(['user'])
                )
            )->addCacheableDependency($entity);
        }
        return AccessResult::allowedIfHasPermission($account, 'view published job entities')
          ->addCacheableDependency($entity);

      case 'publish':
        return AccessResult::allowedIfHasPermission($account, 'edit job entities')
          ->orIf(
            AccessResult::forbiddenIf($entity->getPublishStartTime())
              ->addCacheableDependency($entity)
          );

      case 'withdraw':
        // Either staff or the submitter can withdraw.
        if (!$entity->getPublishEndTime() || $entity->isWithdrawn()) {
          return AccessResult::forbidden()->setCacheMaxAge(0);
        }
        return AccessResult::allowedIfHasPermission($account, 'edit job entities')
          ->orIf(
            AccessResult::allowedIfHasPermission($account, 'edit own job entities')
              ->andIf(
                AccessResult::allowedIf($entity->getOwnerId() == $account->id())
                  ->addCacheContexts(['user'])
              )
              ->addCacheableDependency($entity)
          )
          ->orIf(
            AccessResult::forbiddenIf($entity->isPublished())
              ->addCacheableDependency($entity)
          )
          ->setCacheMaxAge(0);

      case 'update':
        // Users with the edit job permission can edit jobs, or if they were
        // the person who created the job originally.
        return AccessResult::allowedIfHasPermission($account, 'edit job entities')
          ->orIf(AccessResult::allowedIfHasPermission($account, 'edit own job entities')
            ->andIf(AccessResult::allowedIf($entity->getOwnerId() == $account->id())));

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete job entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add job entities');
  }

}
