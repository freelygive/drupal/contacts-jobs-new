<?php

namespace Drupal\contacts_jobs\EventSubscriber;

use Drupal\contacts\Event\UserCancelConfirmationEvent;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber invoked on user cancel confirmation form.
 *
 * Provides additional information, confirmations and errors about the
 * cancellation based on contacts jobs.
 *
 * @package Drupal\contacts_jobs\EventSubscriber
 */
class JobUserCancelConfirmSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * The job entity storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $storage;

  /**
   * JobUserCancelConfirmSubscriber constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->storage = $entity_type_manager->getStorage('contacts_job');
  }

  /**
   * Invoked when a user cancel form is created.
   *
   * @param \Drupal\contacts\Event\UserCancelConfirmationEvent $event
   *   The event representing the user cancel confirmation.
   */
  public function onCancelConfirm(UserCancelConfirmationEvent $event) {
    $user = $event->getUser();

    // Check for any contacts jobs for the event user.
    $jobs = $this->storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $user->id())
      ->execute();
    $count = count($jobs);
    if ($count) {

      $active_count = 0;
      /** @var \Drupal\contacts_jobs\Entity\JobInterface $job */
      foreach ($this->storage->loadMultiple($jobs) as $job) {
        if ($job->isPublished()) {
          $active_count++;
        }
      }

      $params = [
        '%count' => $count,
        '%user' => $user->label(),
        '@active' => $active_count,
      ];
      if ($active_count > 0) {
        $message = $this->formatPlural(
          $count,
          '%user has posted a job vacancy, including @active current',
          '%user has posted %count job vacancies, including @active current',
          $params,
        );
        $event->addError($message, $this->t('Jobs'));
      }
      else {
        $message = $this->formatPlural(
          $count,
          '%user has posted a job vacancy',
          '%user has posted %count job vacancies',
          $params,
        );
        $event->addInfo($message, $this->t('Jobs'));

        $message = $this->formatPlural($count, 'One job will be anonymised.', '@count jobs will be anonymised.');
        $event->addConfirmation($message, $this->t('Jobs'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[UserCancelConfirmationEvent::NAME][] = ['onCancelConfirm'];
    return $events;
  }

  /**
   * Anonymise jobs on user delete.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user just deleted.
   */
  public function onUserDelete(UserInterface $user) : void {
    // Check for any commerce orders for the event user.
    $jobs = $this->storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('uid', $user->id())
      ->execute();

    /** @var \Drupal\contacts_jobs\Entity\JobInterface $job */
    foreach ($this->storage->loadMultiple($jobs) as $job) {
      $job->setOwnerId(0);
      $job->save();
    }
  }

}
