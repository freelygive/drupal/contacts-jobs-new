<?php

namespace Drupal\contacts_jobs\Form;

use Drupal\contacts_jobs\Entity\JobInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Trait to build the job progress bar.
 *
 * @todo Move step declarations into a hook with a cache.
 * @package Drupal\contacts_jobs\Form
 */
trait JobProgressTrait {

  /**
   * The workflow for this form.
   *
   * @var string|null
   */
  protected ?string $workflow = NULL;

  /**
   * The job we are working with.
   *
   * @var \Drupal\contacts_jobs\Entity\JobInterface|null
   */
  protected ?JobInterface $job = NULL;

  /**
   * Get the active step for this form.
   *
   * @return string
   *   The active step.
   */
  abstract protected function getActiveStep(): string;

  /**
   * Helper to return the job interface.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   Returns the related job.
   */
  public function getJob(): JobInterface {
    return $this->job;
  }

  /**
   * Retrieve the workflow from the route where possible.
   *
   * @param \Drupal\Core\Routing\RouteMatchInterface $route_match
   *   The route match.
   */
  protected function getWorkflowFromRoute(RouteMatchInterface $route_match): void {
    // Get the operation if we aren't on this form in isolation.
    if (!$this->getRequest()->query->has('destination')) {
      if ($route_match->getParameter('workflow')) {
        $this->workflow = $route_match->getParameter('workflow');
      }
    }
  }

  /**
   * Build the progress bar.
   *
   * @param array $form
   *   The form array.
   */
  protected function buildProgress(array &$form): void {
    if (!$this->workflow) {
      return;
    }
    $active_step = $this->getActiveStep();

    $form['_progress'] = [
      '#weight' => -999,
      '#theme' => 'item_list__job_progress',
      '#list_type' => 'ol',
      '#items' => [],
      '#attributes' => [
        'class' => ['checkout-progress'],
      ],
    ];

    $payment_step = \Drupal::moduleHandler()
      ->moduleExists('contacts_jobs_commerce');
    switch ($this->workflow) {
      case 'post':
        $form['_progress']['#items']['job'] = $this->t('Create your Job');
        if ($payment_step) {
          $form['_progress']['#items']['pay'] = $this->t('Pay for your advert');
        }
        break;

      case 'edit':
        $form['_progress']['#items']['job'] = $this->t('Edit your Job');
        break;

      case 'repost':
        $form['_progress']['#items']['job'] = $this->t('Edit your Job');
        if ($payment_step) {
          $form['_progress']['#items']['pay'] = $this->t('Pay to repost your advert');
        }
        break;
    }

    $position = 'previous';
    foreach ($form['_progress']['#items'] as $step => $item) {
      if ($step == $active_step) {
        $position = 'current';
        $form['#title'] = $item;
      }

      $form['_progress']['#items'][$step] = [
        '#markup' => $item,
      ];

      $form['_progress']['#items'][$step]['#wrapper_attributes']['class'] = [
        'checkout-progress--step',
        "checkout-progress--step__{$position}",
      ];

      if ($step == $active_step) {
        $position = 'next';
      }
    }
  }

  /**
   * Submit handler to redirect to the next step.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submitNextStep(array $form, FormStateInterface $form_state) {
    if (!$this->workflow) {
      return;
    }

    $form_state->setRedirect('entity.contacts_job.canonical', [
      'contacts_job' => $this->job ? $this->job->id() : $this->getEntity()
        ->id(),
    ]);

    switch ($this->getActiveStep()) {
      case 'job':
        if (\Drupal::moduleHandler()->moduleExists('contacts_jobs_commerce')) {
          $form_state->setRedirect('contacts_jobs_commerce.contacts_job.payment', [
            'contacts_job' => $this->job ? $this->job->id() : $this->getEntity()
              ->id(),
            'workflow' => $this->workflow,
          ]);
        }
        break;
    }
  }

}
