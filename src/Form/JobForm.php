<?php

namespace Drupal\contacts_jobs\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form controller for Job edit forms.
 *
 * @ingroup contacts_jobs
 */
class JobForm extends ContentEntityForm {

  use JobProgressTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * {@inheritdoc}
   */
  protected function getActiveStep(): string {
    return 'job';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): JobForm {

    /** @var static $form */
    $form = parent::create($container);
    $form->currentUser = $container->get('current_user');
    $form->requestStack = $container->get('request_stack');
    $form->setModuleHandler($container->get('module_handler'));

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match, $entity_type_id) {
    $this->getWorkflowFromRoute($route_match);
    return parent::getEntityFromRouteMatch($route_match, $entity_type_id);
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    $this->buildProgress($form, 'job');
    $form = parent::form($form, $form_state);

    /** @var \Drupal\contacts_jobs\Entity\Job $job */
    $job = $this->entity;

    if (isset($form['job_type'])) {
      $type_paid_state = [
        'visible' => [
          ':input[name=job_type]' => ['value' => 'paid'],
        ],
      ];
      if (isset($form['job_salary'])) {
        $form['job_salary']['#states'] = $type_paid_state;
      }
      if (isset($form['job_salary_max'])) {
        $form['job_salary_max']['#states'] = $type_paid_state;
      }
    }

    if (isset($form['job_hours']) && isset($form['job_hours_per_week'])) {
      $form['job_hours_per_week']['#states'] = [
        'visible' => [
          ':input[name=job_hours]' => ['value' => 'part_time'],
        ],
      ];
    }

    // Fix the closing date description.
    if (isset($form['closing'])) {
      $form['closing']['widget'][0]['value']['#description'] = $form['closing']['widget']['#description'];
    }

    // Display a warning if withdrawn.
    if ($this->workflow == 'edit' && $job->isWithdrawn()) {
      \Drupal::messenger()->addWarning($this->t('This job has been withdrawn.'));
    }

    // Show organisations as a select if we don't have global edit permission.
    if (!$this->currentUser->hasPermission('edit job entities')) {
      $options = [];

      // The current organisation.
      /** @var \Drupal\user\UserInterface|null $org */
      if ($org = $this->entity->get('organisation')->entity) {
        $options[$org->id()] = new TranslatableMarkup('@label (current)', [
          '@label' => $org->label(),
        ]);
      }

      /** @var \Drupal\user\UserInterface $user */
      $user = $this->entityTypeManager
        ->getStorage('user')
        ->load($this->currentUser->id());
      $options += $this->getOrganisationOptions($user);

      $form['organisation']['widget'][0]['target_id']['#type'] = 'select';
      $form['organisation']['widget'][0]['target_id']['#options'] = $options;
      $form['organisation']['widget'][0]['target_id']['#size'] = 1;
      if (isset($form['organisation']['widget'][0]['target_id']['#default_value'])) {
        $form['organisation']['widget'][0]['target_id']['#default_value'] = $form['organisation']['widget'][0]['target_id']['#default_value']->id();
      }
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  protected function actions(array $form, FormStateInterface $form_state): array {
    $actions = parent::actions($form, $form_state);
    $actions['submit']['#value'] = $this->t('Save & Next');
    $actions['submit']['#submit'][] = '::submitNextStep';

    if (isset($this->workflow)) {
      unset($actions['delete']);
    }

    $actions['cancel'] = [
      '#type' => 'link',
      '#title' => $this->t('Discard changes'),
      '#attributes' => [
        'class' => ['btn', 'btn-outline-secondary'],
      ],
    ];
    if ($this->getRequest()->query->has('destination')) {
      $actions['cancel']['#url'] = Url::fromUserInput($this->getRequest()->query->get('destination'));
    }
    elseif ($this->entity->isNew()) {
      if ($route = $this->getCancelRoute()) {
        $actions['cancel']['#title'] = $this->t('Cancel');
        $actions['cancel']['#url'] = Url::fromRoute($route);
      }
      else {

        // Something has unset our default cancel route, so remove the button as
        // we have nowhere to send users to on cancel.
        unset($actions['cancel']);
      }
    }
    else {
      $actions['cancel']['#url'] = $this->entity->toUrl();
    }

    return $actions;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    parent::save($form, $form_state);

    /** @var \Drupal\contacts_jobs\Entity\JobInterface $job */
    $job = $this->entity;

    if (!$job->isPublished()) {
      $this->messenger()->addMessage($this->t('Your changes to %job have been saved.', ['%job' => $job->label()]));
    }
    else {
      $this->messenger()->addMessage($this->t('Thank you for posting your job vacancy %job.', [
        '%job' => $job->label(),
      ]));
    }

    $form_state->setRedirect('entity.contacts_job.canonical', ['contacts_job' => $job->id()]);
  }

  /**
   * Get the organisation options for a user.
   *
   * @param \Drupal\user\UserInterface $user
   *   The user.
   *
   * @return array
   *   The options - labels keyed by ID.
   */
  protected function getOrganisationOptions(UserInterface $user): array {
    $options = [];

    if (!$user->isAnonymous()) {
      /** @var \Drupal\contacts_group\Plugin\Field\FieldType\GroupMembershipItem $membership */
      foreach ($user->organisations as $membership_item) {
        /** @var \Drupal\group\GroupMembership $membership */
        $membership = $membership_item->membership;
        $allowed = FALSE;
        foreach ($membership->getRoles() as $role) {
          if ($role->id() == 'contacts_org-member') {
            $allowed = TRUE;
            break;
          }
        }
        if ($allowed) {
          $org = $membership->getGroup()->contacts_org->entity;
          $options[$org->id()] = $org->label();
        }
      }
    }

    asort($options);
    return $options;
  }

  /**
   * Allow altering of route used for cancel buttons in the form.
   *
   * @return mixed|string
   *   The route may be altered using hook_contacts_jobs_alter_cancel_route.
   */
  public function getCancelRoute() {
    $route = 'view.contacts_jobs_list.listings';
    $this->moduleHandler->invokeAll('contacts_jobs_alter_cancel_route', [&$route]);

    return $route;
  }

}
