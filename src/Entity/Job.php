<?php

namespace Drupal\contacts_jobs\Entity;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\user\UserInterface;

/**
 * Defines the Job entity.
 *
 * @ingroup contacts_jobs
 *
 * @ContentEntityType(
 *   id = "contacts_job",
 *   label = @Translation("Job"),
 *   bundle_label = @Translation("Job type"),
 *   handlers = {
 *     "list_builder" = "Drupal\contacts_jobs\JobListBuilder",
 *     "view_builder" = "Drupal\contacts_jobs\JobViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "default" = "Drupal\contacts_jobs\Form\JobForm",
 *       "edit" = "Drupal\contacts_jobs\Form\JobForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "withdraw" = "Drupal\contacts_jobs\Form\JobWithdrawForm",
 *       "publish" = "Drupal\contacts_jobs\Form\JobPublishForm",
 *     },
 *     "access" = "Drupal\contacts_jobs\JobAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\contacts_jobs\JobHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "contacts_job",
 *   admin_permission = "administer job entities",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "type",
 *     "label" = "title",
 *     "uuid" = "uuid",
 *     "uid" = "uid",
 *     "langcode" = "langcode",
 *     "status" = "status",
 *   },
 *   links = {
 *     "collection" = "/admin/content/jobboard",
 *     "add-page" = "/admin/content/jobboard/add",
 *     "add-form" = "/admin/content/jobboard/add/{contacts_job_type}",
 *     "edit-form" = "/admin/content/jobboard/{contacts_job}",
 *     "publish-form" = "/admin/content/jobboard/{contacts_job}/publish",
 *     "delete-form" = "/admin/content/jobboard/{contacts_job}/delete",
 *     "canonical" = "/job/{contacts_job}",
 *     "post-form" = "/job/post/{contacts_job_type}",
 *     "post-existing-form" = "/job/{contacts_job}/post",
 *     "update-form" = "/job/{contacts_job}/edit",
 *     "repost-form" = "/job/{contacts_job}/repost",
 *     "withdraw-form" = "/job/{contacts_job}/withdraw",
 *   },
 *   bundle_entity_type = "contacts_job_type",
 *   field_ui_base_route = "entity.contacts_job_type.edit_form"
 * )
 */
class Job extends ContentEntityBase implements JobInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage, array &$values): void {
    parent::preCreate($storage, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle(string $title): JobInterface {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime(int $timestamp): JobInterface {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): ?int {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): JobInterface {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): JobInterface {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isClosed(): bool {
    $closed = $this->get('closing')->value;
    return $closed !== NULL && $closed < time();
  }

  /**
   * {@inheritdoc}
   */
  public function getClosingTime(): ?int {
    return (int) $this->get('closing')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setClosingTime(?int $timestamp): JobInterface {
    $this->set('closing', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished(): bool {
    if ($this->isWithdrawn() || $this->isClosed()) {
      return FALSE;
    }

    $start = $this->getPublishStartTime();
    if (empty($start)) {
      return FALSE;
    }
    $end = $this->getPublishEndTime();

    $now = \Drupal::service('datetime.time')->getRequestTime();

    return $start <= $now && ($end > $now || is_null($end));
  }

  /**
   * {@inheritdoc}
   */
  public function getPublishStartTime(): ?int {
    return (int) $this->get('publish_start')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublishStartTime(?int $timestamp): JobInterface {
    $this->set('publish_start', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPublishEndTime(): ?int {
    if (!$this->getPublishStartTime()) {
      return NULL;
    }

    // Use the min of publish end (if set) and closing time.
    $times = [];
    if ($closing = $this->getClosingTime()) {
      $times[] = $closing;
    }
    if ($end = $this->get('publish_end')->value) {
      $times[] = $end;
    }

    if (empty($times)) {
      return NULL;
    }

    return min($times) ?: NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function setPublishEndTime(?int $timestamp): JobInterface {
    $this->set('publish_end', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPromoted(): bool {
    if (!$this->isPublished()) {
      return FALSE;
    }

    // If no promoted start then not promoted.
    if ($this->get('promoted_start')->isEmpty()) {
      return FALSE;
    }

    $start = $this->getPromotedStartTime();
    $end = $this->getPromotedEndTime();

    $now = \Drupal::service('datetime.time')->getRequestTime();

    return $start <= $now && ($end > $now || is_null($end));
  }

  /**
   * {@inheritdoc}
   */
  public function getPromotedStartTime(): ?int {
    if (!$this->isPublished()) {
      return NULL;
    }
    return (int) $this->get('promoted_start')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setPromotedStartTime(?int $timestamp): JobInterface {
    $this->set('promoted_start', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getPromotedEndTime(): ?int {
    if (!$this->isPublished() || !$this->getPromotedStartTime()) {
      return NULL;
    }

    // Cannot be promoted beyond the publish end date, so take a max.
    $times = [$this->getPublishEndTime()];
    if ($end = $this->get('promoted_end')->value) {
      $times[] = (int) $end;
    }
    return min($times);
  }

  /**
   * {@inheritdoc}
   */
  public function setPromotedEndTime(?int $timestamp): JobInterface {
    $this->set('promoted_end', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setWithdrawn(bool $is_withdrawn): JobInterface {
    if ($is_withdrawn) {
      $time = time();
      $this->setWithdrawnTime($time);
      if ($this->isPublished()) {
        $this->setPublishEndTime($time);
      }
    }
    else {
      $this->setWithdrawnTime(NULL);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isWithdrawn(): bool {
    return (bool) $this->get('withdrawn')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getWithdrawnTime(): ?int {
    if (!$this->isWithdrawn() || !$this->getPublishStartTime()) {
      return NULL;
    }

    return (int) $this->get('withdrawn')->value;
  }

  /**
   * Sets the Job withdrawn timestamp.
   *
   * @param int|null $timestamp
   *   The Job withdrawn timestamp or NULL to clear.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  protected function setWithdrawnTime(?int $timestamp): JobInterface {
    $this->set('withdrawn', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setLabel(new TranslatableMarkup('Title'))
      ->setSettings([
        'max_length' => 255,
        'text_processing' => 0,
      ])
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Posted by'))
      ->setDescription(new TranslatableMarkup('The username of the content author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 99,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 19,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(new TranslatableMarkup('Created'))
      ->setDescription(new TranslatableMarkup('The time that the job was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(new TranslatableMarkup('Changed'))
      ->setDescription(new TranslatableMarkup('The time that the job was last edited.'));

    $fields['closing'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Deadline'))
      ->setDescription(new TranslatableMarkup('The last date on which applications will be submitted. If this is before the end of the adverts duration, the job will not be visible after this date.'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'timestamp',
        'weight' => 99,
      ])
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp_no_default',
        'weight' => 14,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(TRUE);

    $fields['publish_start'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Publish Date'))
      ->setDescription(new TranslatableMarkup('The first date on which this job will be published.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['publish_end'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Publish End'))
      ->setDescription(new TranslatableMarkup('The time that the job ends publishing.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['promoted_start'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Promoted Start'))
      ->setDescription(new TranslatableMarkup('The time that the job starts promoting.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['promoted_end'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Promoted End'))
      ->setDescription(new TranslatableMarkup('The time that the job ends promoting.'))
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE)
      ->setRequired(FALSE);

    $fields['organisation'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(new TranslatableMarkup('Organisation'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'search_api')
      ->setSetting('handler_settings', [
        'index' => 'contacts_index',
        'conditions' => [['roles', 'crm_org']],
      ])
      ->setRequired(TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'author',
        'weight' => 99,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'weight' => 19,
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => '60',
          'placeholder' => '',
        ],
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['withdrawn'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(new TranslatableMarkup('Withdrawn'))
      ->setDescription(new TranslatableMarkup('The time the job was withdrawn'))
      ->setDisplayConfigurable('form', FALSE)
      ->setDisplayConfigurable('view', FALSE);

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage): void {
    parent::preSave($storage);

    // Ensure we have a published end if we have a published start.
    if (!$this->get('publish_start')->isEmpty()) {
      if ($this->get('publish_end')->isEmpty()) {
        // Use the closing time if not otherwise set.
        $this->setPublishEndTime($this->getClosingTime());
      }
    }
    else {
      $this->setPublishEndTime(NULL);
    }

    // Ensure we have a promoted end if we have a promoted start.
    if (!$this->get('promoted_start')->isEmpty()) {
      if ($this->get('promoted_end')->isEmpty()) {
        // Use the unpublish time if not otherwise set.
        $this->setPromotedEndTime($this->getPublishEndTime());
      }
    }
    else {
      $this->setPromotedEndTime(NULL);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTagsToInvalidate(): array {
    $tags = parent::getCacheTagsToInvalidate();
    return Cache::mergeTags($tags, [
      'contacts_job:user:' . $this->getOwnerId(),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    // If the job has published/promoted start/end in the future, we need to
    // reduce the cache expiry accordingly.
    $now = \Drupal::time()->getRequestTime();
    $expiries = array_filter([
      $this->getPublishStartTime(),
      $this->getPublishEndTime(),
      $this->getPromotedStartTime(),
      $this->getPromotedEndTime(),
    ], fn (?int $expiry) => $expiry && $expiry > $now);
    if ($expiries) {
      return min($expiries) - $now;
    }
    return parent::getCacheMaxAge();
  }

}
