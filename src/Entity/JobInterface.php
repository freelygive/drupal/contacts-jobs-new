<?php

namespace Drupal\contacts_jobs\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Job entities.
 *
 * @ingroup contacts_jobs
 */
interface JobInterface extends ContentEntityInterface, EntityChangedInterface, EntityOwnerInterface {

  /**
   * Gets the Job title.
   *
   * @return string
   *   Title of the Job.
   */
  public function getTitle(): string;

  /**
   * Sets the Job title.
   *
   * @param string $title
   *   The Job title.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  public function setTitle(string $title): JobInterface;

  /**
   * Gets the Job creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Job.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the Job creation timestamp.
   *
   * @param int $timestamp
   *   The Job creation timestamp.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  public function setCreatedTime(int $timestamp): JobInterface;

  /**
   * Whether the job is currently closed.
   *
   * @return bool
   *   Current closed status of job.
   */
  public function isClosed(): bool;

  /**
   * Gets the Job closing timestamp.
   *
   * @return int|null
   *   Closing timestamp of the Job, if there is one.
   */
  public function getClosingTime(): ?int;

  /**
   * Sets the Job closing timestamp.
   *
   * @param int|null $timestamp
   *   The Job closing timestamp or NULL to clear.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  public function setClosingTime(?int $timestamp): JobInterface;

  /**
   * Gets the Job publish start timestamp.
   *
   * @return int|null
   *   Publish start timestamp of the Job, if there is one.
   */
  public function getPublishStartTime(): ?int;

  /**
   * Sets the Job publish start timestamp.
   *
   * @param int|null $timestamp
   *   The Job publish start timestamp or NULL to clear.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  public function setPublishStartTime(?int $timestamp): JobInterface;

  /**
   * Gets the Job publish end timestamp.
   *
   * @return int|null
   *   Publish end timestamp of the Job, if there is one.
   */
  public function getPublishEndTime(): ?int;

  /**
   * Sets the Job publish end timestamp.
   *
   * @param int|null $timestamp
   *   The Job publish end timestamp or NULL to clear.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  public function setPublishEndTime(?int $timestamp): JobInterface;

  /**
   * Whether the job is currently promoted.
   *
   * @return bool
   *   Current promoted status of job.
   */
  public function isPromoted(): bool;

  /**
   * Gets the Job promoted start timestamp.
   *
   * @return int|null
   *   Promoted start timestamp of the Job, if there is one.
   */
  public function getPromotedStartTime(): ?int;

  /**
   * Sets the Job promoted start timestamp.
   *
   * @param int|null $timestamp
   *   The Job promoted start timestamp or NULL to clear.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  public function setPromotedStartTime(?int $timestamp): JobInterface;

  /**
   * Gets the Job promoted end timestamp.
   *
   * @return int|null
   *   Promoted end timestamp of the Job, if there is one.
   */
  public function getPromotedEndTime(): ?int;

  /**
   * Sets the Job promoted end timestamp.
   *
   * @param int|null $timestamp
   *   The Job promoted end timestamp or NULL to clear.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  public function setPromotedEndTime(?int $timestamp): JobInterface;

  /**
   * Whether the job is currently published.
   *
   * @return bool
   *   Current published status of job.
   */
  public function isPublished(): bool;

  /**
   * Sets whether or not the job is withdrawn.
   *
   * @param bool $is_withdrawn
   *   Whether the job should be marked as withdrawn.
   *
   * @return \Drupal\contacts_jobs\Entity\JobInterface
   *   The called Job entity.
   */
  public function setWithdrawn(bool $is_withdrawn): JobInterface;

  /**
   * Whether the job is withdrawn.
   *
   * @return bool
   *   Whether the job is withdrawn.
   */
  public function isWithdrawn(): bool;

  /**
   * Gets the Job withdrawn timestamp.
   *
   * @return int|null
   *   Withdrawn timestamp of the Job, if there is one.
   */
  public function getWithdrawnTime(): ?int;

}
