<?php

namespace Drupal\contacts_jobs\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Job type entity.
 *
 * @ConfigEntityType(
 *   id = "contacts_job_type",
 *   label = @Translation("Job type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\contacts_jobs\JobTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\contacts_jobs\Form\JobTypeForm",
 *       "edit" = "Drupal\contacts_jobs\Form\JobTypeForm",
 *       "delete" = "Drupal\contacts_jobs\Form\JobTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "contacts_job_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "contacts_job",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   config_export = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/contacts_job_type/{contacts_job_type}",
 *     "add-form" = "/admin/structure/contacts_job_type/add",
 *     "edit-form" = "/admin/structure/contacts_job_type/{contacts_job_type}/edit",
 *     "delete-form" = "/admin/structure/contacts_job_type/{contacts_job_type}/delete",
 *     "collection" = "/admin/structure/contacts_job_type"
 *   }
 * )
 */
class JobType extends ConfigEntityBundleBase implements JobTypeInterface {

  /**
   * The Job type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Job type label.
   *
   * @var string
   */
  protected $label;

}
