<?php

namespace Drupal\contacts_jobs\Plugin\Block;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides a job listings controls block.
 *
 * @Block(
 *   id = "contacts_jobs_listings_controls",
 *   admin_label = @Translation("Job Listings Controls"),
 *   category = @Translation("Contacts Jobs")
 * )
 */
class JobListingsControlsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected TimeInterface $time;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected Request $request;

  /**
   * Constructs a new JobListingsControlsBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The current request.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, AccountProxyInterface $current_user, TimeInterface $time, Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->time = $time;
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user'),
      $container->get('datetime.time'),
      $container->get('request_stack')->getCurrentRequest()
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    /** @var \Drupal\views\Entity\View $view */
    $view = $this->entityTypeManager->getStorage('view')->load('contacts_jobs_list');
    if ($view && $display = $view->getDisplay('listings')) {
      if (!empty($display['display_options']['defaults']['sorts'])) {
        $display = $view->getDisplay('default');
      }
      /** @var bool|array $exposed_sort */
      $exposed_sort = FALSE;
      foreach ($display['display_options']['sorts'] as $settings) {
        if ($settings['exposed']) {
          // We can only handle one exposed sort, so set to TRUE and break.
          if ($exposed_sort) {
            $exposed_sort = TRUE;
            break;
          }
          // Track the exposed sort.
          else {
            $exposed_sort = $settings;
          }
        }
      }

      // If we have a single exposed sort and it's the created date, expose as
      // links.
      $supported_sorts = [
        'search_api_index_contacts_job_index:created',
        'search_api_index_contacts_job_index:publish_start',
        'search_api_index_contacts_job_index:publish_end',
      ];
      if (in_array("{$exposed_sort['table']}:{$exposed_sort['field']}", $supported_sorts)) {
        $query = $this->request->query->all();
        $build['sort'] = [
          '#theme' => 'links',
          '#heading' => ['text' => $this->t('Sort by')],
          '#links' => [],
        ];

        $build['sort']['#links']['DESC'] = [
          'title' => $this->t('Newest'),
          'attributes' => [
            'title' => $this->t('Show the newest jobs first'),
          ],
          'url' => Url::fromRoute('view.contacts_jobs_list.listings', [], [
            'query' => [
              'sort_by' => $exposed_sort['field'],
              'sort_order' => 'DESC',
            ] + $query,
          ]),
        ];

        $build['sort']['#links']['ASC'] = [
          'title' => $this->t('Oldest'),
          'attributes' => [
            'title' => $this->t('Show the oldest jobs first'),
          ],
          'url' => Url::fromRoute('view.contacts_jobs_list.listings', [], [
            'query' => [
              'sort_by' => $exposed_sort['field'],
              'sort_order' => 'ASC',
            ] + $query,
          ]),
        ];

        $active_sort_by = $query['sort_by'] ?? array_keys($display['display_options']['sorts'])[0];
        if ($active_sort_by == $exposed_sort['field']) {
          $active_sort_order = $query['sort_order'] ?? $exposed_sort['order'];
          $build['sort']['#links'][$active_sort_order]['attributes']['class'][] = 'is-active';
        }
      }
    }

    $build['links'] = [
      '#theme' => 'links',
      '#set_active_class' => FALSE,
      '#cache' => [
        'contexts' => ['user'],
      ],
    ];

    $build['links']['#links']['post'] = [
      'title' => $this->t('Post a job'),
      'url' => Url::fromRoute('entity.contacts_job.post_form'),
    ];

    if (!$this->currentUser->hasPermission('post new jobs')) {
      // Use the login link with a redirect if not logged in.
      if ($this->currentUser->isAnonymous()) {
        $build['links']['#links']['post']['url'] = Url::fromRoute('user.login', [], [
          'query' => [
            'destination' => $build['links']['#links']['post']['url']->toString(),
          ],
        ]);
      }
      // Otherwise don't show the link.
      else {
        unset($build['links']['#links']['post']);
      }
    }

    if ($this->currentUser->isAnonymous()) {
      return $build;
    }

    $build['links']['#cache']['tags'][] = 'contacts_job:user:' . $this->currentUser->id();

    if ($count = $this->getJobCount('active')) {
      $build['links']['#links']['my-active'] = [
        'title' => $this->t('My active jobs'),
        'url' => Url::fromRoute('view.contacts_jobs_list.my_active'),
        'job_count' => $count,
      ];
    }

    if ($count = $this->getJobCount('expired')) {
      $build['links']['#links']['my-expired'] = [
        'title' => $this->t('My past jobs'),
        'url' => Url::fromRoute('view.contacts_jobs_list.my_expired'),
        'job_count' => $count,
      ];
    }

    if ($count = $this->getJobCount('pending')) {
      $build['links']['#links']['my-pending'] = [
        'title' => $this->t('My pending jobs'),
        'url' => Url::fromRoute('view.contacts_jobs_list.my_pending'),
        'job_count' => $count,
      ];
    }

    return $build;
  }

  /**
   * Get a list of jobs for the current user in the given status.
   *
   * @param string $status
   *   One of 'active', 'expired' or 'pending'.
   *
   * @return int
   *   The number of jobs.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getJobCount(string $status): int {
    $query = $this->entityTypeManager
      ->getStorage('contacts_job')
      ->getQuery()
      ->count();

    $query->condition('uid', $this->currentUser->id());
    $query->accessCheck();

    switch ($status) {
      case 'active':
        $query->condition('closing', $this->time->getRequestTime(), '>=');
        $query->condition('publish_start', $this->time->getRequestTime(), '<');
        $or = $query->orConditionGroup();
        $or->notExists('publish_end');
        $or->condition('publish_end', $this->time->getRequestTime(), '>=');
        $query->condition($or);
        break;

      case 'expired':
        $query->condition('publish_start', $this->time->getRequestTime(), '<');
        $or = $query->orConditionGroup();
        $or->condition('closing', $this->time->getRequestTime(), '<');
        $or->condition('publish_end', $this->time->getRequestTime(), '<');
        $query->condition($or);
        break;

      case 'pending':
        $or = $query->orConditionGroup();
        $or->notExists('publish_start');
        $or->condition('publish_start', $this->time->getRequestTime(), '>');
        $query->condition($or);
        break;

      default:
        return 0;
    }

    return $query->execute();
  }

}
